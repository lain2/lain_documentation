# Configuracion de entorno Backend

## Requerimientos básico

- python3
- pip 
- virtualenv
- nodejs
- npm

## Entorno Virtual

Los entornos virtuales de python es un mecanismos que permite gestionar paquetes de python en un 
espacio aislados donde se puede instalar distintas versiones de programas y paquetes de python.

Para realizar la instalacion se utilizara virtualenv el cual se instala mediante el gestor de paquetes pip. 

Primero debemos instalar pip 
```bash
$ sudo apt-get install python3-pip
```
Posteriormente instalamos virtualenv con el comando
```bash 
$ sudo pip3 install virtualenv
```

Ahora para crear el entorno virtual ejecutamos el siguiente comando:

```bash
$ virtualenv lain_env
```
>lain_env es el nombre del entorno, por lo cual puede poner el nombre que desee

Ahora debemos activar el entorno.

```bash
$ source lain_env/bin/activate
```
Para validar que el entorno se encuentra activo podemos ejecutar el siguiente comando y el 
path que nos genere debe ser indicando a la carpeta que contiene el entorno virtual.

```bash
$ which python
```
> En este caso /home/user/lain_env/bin/python

En caso que necesitemos desactivar el entorno de python para usar otro empleamos el comando:
```bash
$ deactivate
```

## Poetry 

Para poder gestionar las dependencias de un proyecto existen multiples formas, pero para este 
proyecto se empleara [poetry](https://python-poetry.org/). Este paquete permite empaquetar y 
mantener las dependencias y la configuracion de librerias de forma sencilla.

Para instalarlo debemos ejecutar:
```bash
$ pip install poetry
```

## Creación del proyecto

Primero procederemos a crear el directorio que contendra todo el proyecto. Este proyecto tendra de nombre lain_backend. Cabe aclarar que esto lo realizaremos donde nos quede mas comodo, pero no lo haremos dentro de la carpeta que contiene nuestro entorno. Una practica que recomiendo es que tenga una carpeta con todos los entornos de python que necesite usar y otra para sus proyectos personales.

```bash
$ mkdir lain_backend && cd lain_backend
```

Una vez realizado esto y posicionado dentro de la carpeta debemos inicializar el proyecto con poetry
para poder realizar la instalacion de todas las librerias que necesitaremos para el proyecto.

```bash
$ poetry init
```

Este comando ejecutara un cli creara un archivo pyproject.toml, el cual posee toda la informacion
del proyecto y sus dependencias. En la imagen se muestra las opciones que marco para este proyecto. Cabe aclarar que las dependencias se pueden agregar en este paso, o posteriormente como realizare mas adelante.

![image](https://gitlab.com/lain2/lain_documentation/-/blob/master/imgs/0001-entorno.png)


A continuacion agregaremos las librerias que necesitaremos, primero agregaremos las de produccion y luego las necesarias para desarrollo.

## Produccion
Las dependencias de produccion son aquellas quer requeriremos para que el producto una vez puesto 
en produccion funcione.

	- uvicorn 
	- fastapi
	- requests 
	- pydantic
	- guinicorn
	- alembic
	- psycopg2-binary
	- sqlalchemy
	- async-exit-stack
	- async-generator

Para realizar la instalacion de estas dependencias debemos ejecutar el comando 

```bash
$ poetry add nombreDependecia
```
> Donde nombreDependecia se debe reemplazar por la que necesites.

Una vez ejecutado ese se realizara la instalacion de la dependencia y se agregara al archivo 
pyproject.toml, tambien podemos especificar la version de la dependencia. Otra forma es agregar 
directamente estas dependencias en el archivo pyproject.toml especificando la version, eliminamos 
el archivo poetry.lock y luego ejecutamos 

```bash
$ rm poetry.lock
$ poetry install 
```

## Desarrollo
Estas dependencias solo serviran para cuando estemos construyendo el software. Por lo que cuando 
desplegemos en produccion el servidor no realizara la instalacion de las mismas.

	- mypy
	- black
	- isort
	- autoflake
	- flake8
	- pytest
	- pytest-cov
	- flake8-import-order
	- flake8-builtins
	- flake8-blind-except
	- flake8-logging-format
	- flakehell
	- flake8-docstrings
	- flake8-rst-docstrings
	- Sphinx
	- sphinx-rtd-theme

# Estructuracion del proyecto
En esta seccion crearemos se crearan todos los directorios y configuraciones necesarias para
poder empezar el desarrollo


## Alembic
Es una herramienta de migración de bases de datos para su uso con el kit de herramientas SQLAlchemy
para python. Permitira la generacion de entidades en la base de datos mediante la creacion de 
clases en python. Debemos ejecutar el comando que se muestra a continuacion

```bash
$ poetry run alembic init alembic
```

Esto generara un directorio que contiene un script de configuracion y permite llevar un versionado
de las migraciones y tambien  se genera un archivo alembic.ini que tambien posee parametros de 
configuración.

En el archivo alembic.ini debemos comentar los siguientes parametros para que tomes los del archivo
de configuracion alembic/env.py:

```
prepend_sys_path = .
sqlalchemy.url = driver://user:pass@localhost/dbname
```

En el archivo alembic/env.py se debe reemplazar el codigo existente por el siguiente:

```python
	import os
	import sys

	from logging.config import fileConfig
	from sqlalchemy import engine_from_config
	from sqlalchemy import pool
	from alembic import context

	config = context.config

	fileConfig(config.config_file_name)

	sys.path = ['', '../app'] + sys.path[1:]


	from app.data.db.base import Base
	from app.config.config import settings

	target_metadata = Base.metadata



	def get_url():
			server: str = settings.POSTGRES_SERVER
			user: str = settings.POSTGRES_USER
			password: str = settings.POSTGRES_PASSWORD
			db: str = settings.POSTGRES_DB
			url = f"postgresql://{user}:{password}@{server}/{db}"
			return url

	def run_migrations_offline():
			url = get_url()
			context.configure(
					url=url,
					target_metadata=target_metadata,
					literal_binds=True,
					compare_type=True
			)

			with context.begin_transaction():
					context.run_migrations()


	def run_migrations_online():

			configuration = config.get_section(config.config_ini_section)
			configuration["sqlalchemy.url"] = get_url()
			connectable = engine_from_config(
					configuration, prefix="sqlalchemy.", poolclass=pool.NullPool,
			)

			with connectable.connect() as connection:
					context.configure(
							connection=connection, target_metadata=target_metadata, compare_type=True
					)
					with context.begin_transaction():
							context.run_migrations()


	if context.is_offline_mode():
			run_migrations_offline()
	else:
			run_migrations_online()
```
El código lo que hace es tomar los datos para conectarse a la db de un archivo de configuracion
que crearemos mas adelante. Tambien establece el directorio de donde se extraera las clases de 
las cuales crearemos nuestras entidades.

## App
Ahora procederemos a crear el directorio app, este es el que contendra en pocas palabras todo el 
codigo de la aplicación y los test. Creamos primero el directorio y luego le creamos el archivo
__init__.py, que indica que es un modulo de python.

```bash 
$ mkdir app && touch app/__init__.py
```
Ahora se crearan los siguientes paquetes

### config
Este paquete contiene la configuracion de las variables como la base de datos, las rutas bases y
nombre del server. Dentro posee el archivo config el cual dependiendo el entorno que queramos 
ejecutar deberemos inicializar en un valor u otro. Y posee archivos independientes que tienen 
la configuracion del entorno.

```bash 
$ mkdir config && touch __init__.py 
$ cd config
```

Como en esta situacion quiero dos entornos, uno de desarrollo y otro de produccion entonces
crearemos dos clases que poseeran estos datos de configuracion y luego seran llamados en el 
archivo config dependiendo la necesidad.

```bash
$  touch dev_config.py prod_config.py config.py
```

Dentro del archivo dev_config y prod_config colocaremos el mismo contenido pero cambiara el nombre
de la clase.

```python
	import secrets
	import os
	from typing import Any, Dict, List, Optional, Union
	from pydantic import (AnyHttpUrl, BaseSettings, EmailStr, HttpUrl, PostgresDsn,
												validator)

	class DevConfig(BaseSettings):
			ENVIROMENT: str = "DEVELOPMENT"
			API_V1_STR: str = "/api/v1/developer"
			ROUTE: str = "/api/v1/developer"
			SECRET_KEY: str = secrets.token_urlsafe(32)

			NEURAL_USERNAME: str = "example@gmail.com"
			NEURAL_PASSWORD: str = "secret"
			ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8

			SERVER_NAME: str = "Lain Development"
			SERVER_HOST: AnyHttpUrl = "http://localhost:8000"
			BACKEND_CORS_ORIGINS   = ["http://localhost", "http://localhost:3000"]

			@validator("BACKEND_CORS_ORIGINS", pre=True)
			def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
					if isinstance(v, str) and not v.startswith("["):
							return [i.strip() for i in v.split(",")]
					elif isinstance(v, (list, str)):
							return v
					raise ValueError(v)

			PROJECT_NAME: str = "Lain Backend development"
			SENTRY_DSN: Optional[HttpUrl] = ""

			@validator("SENTRY_DSN", pre=True)
			def sentry_dsn_can_be_blank(cls, v: str) -> Optional[str]:
					if len(v) == 0:
							return None
					return v

			POSTGRES_SERVER: str = "localhost"
			POSTGRES_USER: str = "postgres"
			POSTGRES_PASSWORD: str = "admin"
			POSTGRES_DB: str = "db_name"
			SQLALCHEMY_DATABASE_URI: Optional[PostgresDsn] = None

			@validator("SQLALCHEMY_DATABASE_URI", pre=True)
			def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
					if isinstance(v, str):
							return v
					return PostgresDsn.build(
							scheme="postgresql",
							user=values.get("POSTGRES_USER"),
							password=values.get("POSTGRES_PASSWORD"),
							host=values.get("POSTGRES_SERVER"),
							path=f"/{values.get('POSTGRES_DB') or ''}"
					)

			class Config:
					case_sensitive = True

	dev = DevConfig()
```

y el archivo config tendra el siguiente código:
```python
	import secrets
	import os

	from .dev_config import dev
	# from .prod_config import prod

	from pydantic import (AnyHttpUrl, BaseSettings, EmailStr,
												HttpUrl, PostgresDsn,validator)


	settings = dev
	# settings = prod
```
De esta forma si necesitamos ejecutar el codigo en produccion, lo unico que debemos hacer es cambiar
la linea de importacion de prod_config y el settings.


### core

El core es un modulo que contendra toda la logica de negocio de nuestra aplicacion, no tendra 
información de como se presentara o almacenara la informacion, solo posee informacion de las reglas
que se deben respertar del negocio.

